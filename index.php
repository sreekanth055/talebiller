<?php
 include 'config.php';
 ?>
<?php
session_start();
if(isset($_POST['submit']))
{ 
    $username = $_POST['username'];
    $password = $_POST['password'];
    $st=$conn->query("SELECT * FROM vm_shopprofile WHERE sp_username='$username' AND sp_password='$password' AND sp_isactive='0'");
    
    if($st->num_rows > 0)
    {
        $row=$st->fetch_assoc();
        $_SESSION['admin'] = $row["sp_shopid"];
        $_SESSION['name'] = $row["sp_shopname"];
        $_SESSION['user'] = $row["sp_username"];
        $_SESSION['stcode'] = $row["sp_stcode"];
        if($row["sp_acnttype"]==0)
        {
            $_SESSION["startdate"]=$row["sp_adddate"];
        }
        header('Location: cnr/dashboard.php');
    }
    else
    {
      header('Location:index.php?fail=failed');    
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Store Biller Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="assets/vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="assets/tables/datatables.net-bs4/css/dataTables.bootstrap4.min.css">    
    
  <!-- endinject -->

  <!-- inject:css -->
  <link rel="stylesheet" href="assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="assets/images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper">
      <div class="row">
        <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-full-bg">
          <div class="row w-100">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-dark text-left p-5">
                <h2>Login</h2>
                <h4 class="font-weight-light">Login here</h4>
                <form class="pt-5" method="post" action="<?=$_SERVER['PHP_SELF']?>">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                    <i class="mdi mdi-account"></i>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    <i class="mdi mdi-eye"></i>
                  </div>
                  <div class="mt-5">
                   <input type="submit" name="submit" class="btn btn-block btn-warning btn-lg font-weight-medium" value="submit"> 
                  </div>
                  <div class="mt-3 text-center">
                    <a href="#" class="auth-link text-white">Forgot password?</a>
                  </div>                 
                </form>
              </div>
<!--  <div style="margin-left:5px; color:#fffff ">
        <b><span style=" color:#309;">Translate to</span></b>
        <a href="mal/" ><b><i style="color:#F00;">Malayalam</i></b></a>        
        </div> -->

            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- row ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/hoverable-collapse.js"></script>
  <script src="assets/js/misc.js"></script>
  <script src="assets/js/settings.js"></script>
  <script src="assets/js/todolist.js"></script>
  <!-- endinject -->
</body>


</html>
