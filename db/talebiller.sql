-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2020 at 07:22 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usebiller_akbar`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator_account_name`
--

CREATE TABLE `administrator_account_name` (
  `refid` int(11) NOT NULL,
  `acc_name` varchar(30) DEFAULT '',
  `acc_head` enum('bs','tr','pl') DEFAULT 'bs',
  `group_head` enum('asset','liability','debit','credit') DEFAULT 'asset',
  `other_details` text,
  `act_group_head` varchar(50) DEFAULT NULL,
  `opening_balance` bigint(15) DEFAULT '0',
  `opening_balance_type` enum('debit','credit') DEFAULT NULL,
  `closing_balance` bigint(15) DEFAULT '0',
  `closing_balance_type` enum('debit','credit') DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `backup` char(1) DEFAULT NULL,
  `acc_updatedtime` date DEFAULT NULL,
  `acnt_branch` varchar(100) DEFAULT NULL,
  `finyear` int(100) DEFAULT NULL,
  `isactive` int(11) DEFAULT '0',
  `note` varchar(200) DEFAULT NULL,
  `bill_id` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_account_name`
--

INSERT INTO `administrator_account_name` (`refid`, `acc_name`, `acc_head`, `group_head`, `other_details`, `act_group_head`, `opening_balance`, `opening_balance_type`, `closing_balance`, `closing_balance_type`, `status`, `backup`, `acc_updatedtime`, `acnt_branch`, `finyear`, `isactive`, `note`, `bill_id`, `user_id`) VALUES
(1, 'CASH', 'bs', 'asset', '', '7', 484185, 'debit', 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(2, 'CUSTOMER', 'bs', 'liability', '', '1', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(4, 'SALES RETURN', 'tr', 'debit', '', '9', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(5, 'SALES', 'tr', 'credit', '', '9', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(6, 'PURCHASE', 'tr', 'debit', '', '10', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(13, 'BANK', 'bs', 'asset', '', '8', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(33, 'INPUT TAX', 'bs', 'asset', NULL, '11', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(34, 'OUTPUT TAX', 'bs', 'asset', NULL, '11', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', '', 1),
(42, 'SALARY', 'bs', 'asset', NULL, '13', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(36, 'COOLIE', 'bs', 'asset', NULL, '12', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(37, 'DISCOUNT', 'bs', 'asset', NULL, '12', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(38, 'CASH SALE', 'bs', 'asset', NULL, '9', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(39, 'CREDIT SALE', 'bs', 'asset', NULL, '9', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(41, 'TAXABLE ', 'bs', 'asset', NULL, '11', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(43, 'CAPITAL', 'bs', 'asset', NULL, '3', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(46, 'PURCHASE RETURN', 'bs', 'asset', NULL, '10', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 4, 0, '', NULL, 1),
(47, 'GIRISH', 'bs', 'asset', 'GIRISH', '2', 680, 'debit', 0, NULL, NULL, NULL, '2020-03-13', '1', 0, 0, NULL, NULL, 1),
(48, 'anish', 'bs', 'asset', NULL, '8', 1200, NULL, 2400, NULL, NULL, NULL, NULL, NULL, 1, 0, 'radnom test', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `administrator_daybook`
--

CREATE TABLE `administrator_daybook` (
  `refid` int(10) NOT NULL,
  `ad_branchid` varchar(100) DEFAULT NULL,
  `dayBookDate` date DEFAULT '0000-00-00',
  `debit` varchar(50) DEFAULT '',
  `credit` varchar(50) DEFAULT '',
  `dayBookContra` enum('Y','N') DEFAULT 'Y',
  `dayBookAmount` double DEFAULT '0',
  `description` text,
  `status` char(1) DEFAULT '',
  `backup` char(1) DEFAULT '',
  `billid` varchar(100) DEFAULT NULL,
  `finyear` varchar(100) DEFAULT NULL,
  `bill_id` varchar(100) DEFAULT NULL,
  `mode` int(100) DEFAULT '0',
  `dr_cr` varchar(100) DEFAULT '0',
  `bill_amnt` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_daybook`
--

INSERT INTO `administrator_daybook` (`refid`, `ad_branchid`, `dayBookDate`, `debit`, `credit`, `dayBookContra`, `dayBookAmount`, `description`, `status`, `backup`, `billid`, `finyear`, `bill_id`, `mode`, `dr_cr`, `bill_amnt`, `user_id`) VALUES
(1, '1', '2020-03-10', '1', '', 'Y', 12, 'Customer Balance Payment - Receipt No:1 ', '', '', NULL, '1', NULL, 1, 'C', '', 1),
(2, '1', '2020-03-11', '1', '', 'Y', 2, 'Customer Balance Payment - Receipt No:2 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(3, '1', '2020-03-11', '1', '', 'Y', 700, 'Customer Balance Payment - Receipt No:3 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(4, '1', '2020-03-12', '1', '', 'Y', 20, 'Customer Balance Payment - Receipt No:4 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(5, '1', '2020-03-12', '1', '', 'Y', 4, 'Customer Balance Payment - Receipt No:5 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(6, '1', '2020-03-13', '0', '2', 'Y', 680, 'OPENING BALANCE', '', '', NULL, '', '', 1, 'D', '', 1),
(7, '1', '2020-03-13', '1', '47', 'Y', 12, 'Customer Balance Payment - Receipt No:6 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(8, '1', '2020-03-13', '1', '47', 'Y', 6, 'Customer Balance Payment - Receipt No:7 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(9, '1', '2020-03-13', '1', '47', 'Y', 8, 'Customer Balance Payment - Receipt No:8 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(10, '1', '2020-03-13', '1', '47', 'Y', 14, 'Customer Balance Payment - Receipt No:9 ', '', '', NULL, '', NULL, 1, 'C', '', 1),
(11, '1', '2020-03-16', '', '1', 'Y', 10, 'Supplier Balance Payment - Voucher No:1', '', '', NULL, '', NULL, 1, 'D', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vm_billentry`
--

CREATE TABLE `vm_billentry` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) NOT NULL,
  `be_vehicle_number` varchar(10) NOT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float DEFAULT NULL,
  `be_gtotal` varchar(100) NOT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) NOT NULL,
  `be_paydate` datetime NOT NULL,
  `be_balance` int(11) NOT NULL,
  `be_customerid` int(11) NOT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `be_oldbal` varchar(100) NOT NULL,
  `be_totvat` float NOT NULL,
  `be_debitid` varchar(100) NOT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) NOT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL,
  `finyear` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_billentry`
--

INSERT INTO `vm_billentry` (`be_billid`, `user_id`, `be_billnumber`, `be_customername`, `be_customermobile`, `be_customer_tin_num`, `be_vehicle_number`, `be_billdate`, `be_total`, `be_gtotal`, `be_paidamount`, `be_paymethod`, `be_note`, `be_updateddate`, `be_updatedby`, `be_isactive`, `be_discount`, `be_mode`, `be_paydate`, `be_balance`, `be_customerid`, `be_coolie`, `be_oldbal`, `be_totvat`, `be_debitid`, `be_creditid`, `be_statecode`, `finyear`) VALUES
(1, '1', 1, '', '', 'dfdsfkdnk4394093043', 'KL04SD2324', '2020-03-10 01:41:00', 4340, '4340', 4500, NULL, NULL, '2020-03-10 01:43:32', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', -160, 2, '0', '290', 0, '', '', 'KL', 0),
(2, '1', 2, '', '', 'jfd4434j35454354353', 'sdfsgbgssf', '2020-03-11 16:09:00', 3700, '3700', 3700, NULL, NULL, '2020-03-11 16:10:49', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 3, '0', '800', 0, '', '', 'KL', 0),
(3, '1', 3, '', '', 'jfd4434j35454354353', 'KL04ER3430', '2020-03-11 16:11:00', 6300, '6300', 5000, NULL, NULL, '2020-03-11 16:12:41', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 1300, 3, '0', '0', 0, '', '', 'KL', 0),
(4, '1', 4, '', '', 'rdwdfjwi4942399090', 'KLGH3470', '2020-03-13 16:31:00', 4210, '4210', 4210, NULL, NULL, '2020-03-13 16:33:47', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 4, '0', '640', 0, '', '', 'KL', 0),
(5, '1', 5, '', '', 'jfd4434j35454354353', 'KL07DS2342', '2020-03-13 17:34:00', 3790, '4390', 4390, NULL, NULL, '2020-03-13 17:36:12', NULL, 0, 50, 'sales', '0000-00-00 00:00:00', 0, 3, '0', '600', 0, '', '', 'KL', 0),
(6, '1', 6, '', '', 'dfdsfkdnk4394093043', 'KL06SD3434', '2020-03-17 20:40:00', 2698, '2698', 2698, NULL, NULL, '2020-03-17 20:40:58', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 2, '0', '-160', 0, '', '', 'KL', 0),
(7, '1', 7, 'arun', '9039430534', 'sadsfwe543534', 'KL05S8787', '2020-03-17 20:42:00', 5100, '5100', 5100, NULL, NULL, '2020-03-17 20:43:25', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', 'KL', 0),
(8, '1', 8, '', '', 'dfdsfkdnk4394093043', 'KL04ER3430', '2020-03-18 10:31:00', 3230, '3230', 3230, NULL, NULL, '2020-03-18 10:31:50', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 2, '0', '0', 0, '', '', 'KL', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_billitems`
--

CREATE TABLE `vm_billitems` (
  `bi_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `bi_billid` int(11) DEFAULT NULL,
  `bi_productid` int(11) DEFAULT NULL,
  `bi_price` float DEFAULT NULL,
  `bi_quantity` float DEFAULT NULL,
  `bi_taxamount` float DEFAULT NULL,
  `bi_discount` float DEFAULT NULL,
  `bi_discount_amount` float NOT NULL,
  `bi_total` float DEFAULT NULL,
  `bi_updatedon` datetime DEFAULT NULL,
  `bi_isactive` int(11) DEFAULT '0',
  `bi_vatamount` float DEFAULT NULL,
  `bi_vatper` float DEFAULT NULL,
  `be_coolie` varchar(100) DEFAULT NULL,
  `bi_sgst` float DEFAULT NULL,
  `bi_sgst_amt` float DEFAULT NULL,
  `bi_cgst` float DEFAULT NULL,
  `bi_cgst_amt` float DEFAULT NULL,
  `bi_igst` float DEFAULT NULL,
  `bi_igst_amt` float DEFAULT NULL,
  `bi_billdate` datetime DEFAULT NULL,
  `bi_purprice` float(10,2) DEFAULT NULL,
  `bi_disc` float(10,2) DEFAULT NULL,
  `bi_prenet` float DEFAULT NULL,
  `finyear` int(100) DEFAULT NULL,
  `bi_totcomm` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_billitems`
--

INSERT INTO `vm_billitems` (`bi_billitemid`, `user_id`, `bi_billid`, `bi_productid`, `bi_price`, `bi_quantity`, `bi_taxamount`, `bi_discount`, `bi_discount_amount`, `bi_total`, `bi_updatedon`, `bi_isactive`, `bi_vatamount`, `bi_vatper`, `be_coolie`, `bi_sgst`, `bi_sgst_amt`, `bi_cgst`, `bi_cgst_amt`, `bi_igst`, `bi_igst_amt`, `bi_billdate`, `bi_purprice`, `bi_disc`, `bi_prenet`, `finyear`, `bi_totcomm`) VALUES
(1, '1', 1, 7, 1200, 1, 1016.95, 0, 0, 1200, '2020-03-10 01:43:32', 0, NULL, NULL, NULL, 9, 91.525, 9, 91.525, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(2, '1', 1, 9, 780, 1, 780, 0, 0, 780, '2020-03-10 01:43:32', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(3, '1', 1, 4, 990, 1, 838.98, 0, 0, 990, '2020-03-10 01:43:32', 0, NULL, NULL, NULL, 9, 75.51, 9, 75.51, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(4, '1', 1, 12, 1370, 1, 1370, 0, 0, 1370, '2020-03-10 01:43:32', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(5, '1', 2, 3, 1250, 1, 1116.07, 0, 0, 1250, '2020-03-11 16:10:49', 0, NULL, NULL, NULL, 6, 66.965, 6, 66.965, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(6, '1', 2, 4, 2450, 1, 2076.27, 0, 0, 2450, '2020-03-11 16:10:49', 0, NULL, NULL, NULL, 9, 186.865, 9, 186.865, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(7, '1', 3, 7, 3400, 1, 2881.36, 0, 0, 3400, '2020-03-11 16:12:41', 0, NULL, NULL, NULL, 9, 259.32, 9, 259.32, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(8, '1', 3, 1, 2900, 1, 2265.63, 0, 0, 2900, '2020-03-11 16:12:41', 0, NULL, NULL, NULL, 14, 317.19, 14, 317.19, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(9, '1', 4, 6, 560, 1, 533.33, 0, 0, 560, '2020-03-13 16:33:47', 0, NULL, NULL, NULL, 2.5, 13.335, 2.5, 13.335, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(10, '1', 4, 9, 1350, 1, 1350, 0, 0, 1350, '2020-03-13 16:33:47', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(11, '1', 4, 5, 2300, 1, 2190.48, 0, 0, 2300, '2020-03-13 16:33:47', 0, NULL, NULL, NULL, 2.5, 54.76, 2.5, 54.76, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(12, '1', 5, 7, 2450, 1, 2076.27, 0, 0, 2450, '2020-03-13 17:36:12', 0, NULL, NULL, NULL, 9, 186.865, 9, 186.865, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(13, '1', 5, 12, 1340, 1, 1340, 0, 0, 1340, '2020-03-13 17:36:12', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(14, '1', 6, 5, 1699, 1, 1618.1, 0, 0, 1699, '2020-03-17 20:40:58', 0, NULL, NULL, NULL, 2.5, 40.45, 2.5, 40.45, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(15, '1', 6, 1, 999, 1, 780.47, 0, 0, 999, '2020-03-17 20:40:58', 0, NULL, NULL, NULL, 14, 109.265, 14, 109.265, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(16, '1', 7, 5, 1250, 1, 1190.48, 0, 0, 1250, '2020-03-17 20:43:25', 0, NULL, NULL, NULL, 2.5, 29.76, 2.5, 29.76, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(17, '1', 7, 12, 2400, 1, 2400, 0, 0, 2400, '2020-03-17 20:43:25', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(18, '1', 7, 13, 1450, 1, 1450, 0, 0, 1450, '2020-03-17 20:43:25', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(19, '1', 8, 4, 1340, 1, 1135.59, 0, 0, 1340, '2020-03-18 10:31:50', 0, NULL, NULL, NULL, 9, 102.205, 9, 102.205, 0, 0, NULL, NULL, NULL, NULL, NULL, ''),
(20, '1', 8, 13, 1890, 1, 1890, 0, 0, 1890, '2020-03-18 10:31:50', 0, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `vm_catogory`
--

CREATE TABLE `vm_catogory` (
  `ca_categoryid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `ca_categoryname` varchar(200) DEFAULT NULL,
  `ca_vat` varchar(200) DEFAULT NULL,
  `ca_isactive` int(11) DEFAULT '0',
  `ca_updatedtime` datetime DEFAULT NULL,
  `ca_note` longtext,
  `finyear` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_catogory`
--

INSERT INTO `vm_catogory` (`ca_categoryid`, `user_id`, `ca_categoryname`, `ca_vat`, `ca_isactive`, `ca_updatedtime`, `ca_note`, `finyear`) VALUES
(1, '1', 'TAX@5%', '5', 0, '2019-01-04 03:13:06', NULL, 0),
(2, '1', 'TAX@12%', '12', 0, '2019-01-05 05:41:34', NULL, 0),
(3, '1', 'GST@18%', '18', 0, '2019-01-05 05:32:11', NULL, 0),
(4, '1', 'GST@28%', '28', 0, '2019-01-05 05:41:18', NULL, 0),
(5, '1', 'GST@32%', '32', 0, '2019-11-23 12:44:11', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_customer`
--

CREATE TABLE `vm_customer` (
  `cs_customerid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cs_billnumber` int(11) DEFAULT NULL,
  `cs_customername` varchar(600) DEFAULT NULL,
  `cs_customerphone` varchar(100) DEFAULT NULL,
  `cs_address` varchar(1000) DEFAULT NULL,
  `cs_email` varchar(400) DEFAULT NULL,
  `cs_tin_number` varchar(100) DEFAULT NULL,
  `cs_payment` float DEFAULT NULL,
  `cs_discount` float DEFAULT NULL,
  `cs_paid` float DEFAULT NULL,
  `cs_balance` float DEFAULT NULL,
  `cs_paymethod` varchar(250) DEFAULT NULL,
  `cs_note` longtext,
  `cs_updateddate` datetime DEFAULT NULL,
  `cs_updatedby` varchar(250) DEFAULT NULL,
  `cs_isactive` int(11) DEFAULT '0',
  `cs_acntid` varchar(100) NOT NULL,
  `cs_statecode` varchar(1000) DEFAULT NULL,
  `cs_category` varchar(200) NOT NULL,
  `cs_regno` varchar(200) NOT NULL,
  `finyear` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_customer`
--

INSERT INTO `vm_customer` (`cs_customerid`, `user_id`, `cs_billnumber`, `cs_customername`, `cs_customerphone`, `cs_address`, `cs_email`, `cs_tin_number`, `cs_payment`, `cs_discount`, `cs_paid`, `cs_balance`, `cs_paymethod`, `cs_note`, `cs_updateddate`, `cs_updatedby`, `cs_isactive`, `cs_acntid`, `cs_statecode`, `cs_category`, `cs_regno`, `finyear`) VALUES
(1, 1, NULL, 'harish', '89210343480', 'street 6, new lane, santhi nagar, EKM', 'harish@mail.com', 'djsfj789789dsaf9798', NULL, NULL, NULL, 82, NULL, NULL, NULL, NULL, 0, '', 'KL', '', '', 0),
(2, 1, NULL, 'mithun', '9989535350', 'new address, new location', 'mithun@mail.com', 'dfdsfkdnk4394093043', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '', 'KL', '', '', 0),
(3, 1, NULL, 'jaleel', '9043523550', 'new address, new street , new location', 'jaleel@mail.com', 'jfd4434j35454354353', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '', 'KL', '', '', 0),
(4, 1, NULL, 'girish', '9093404233', 'some new address, some new location', 'girish@mail.com', 'rdwdfjwi4942399090', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '47', 'KL', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_customeritemprice`
--

CREATE TABLE `vm_customeritemprice` (
  `cp_id` int(11) NOT NULL,
  `cp_customer_id` varchar(10) DEFAULT NULL,
  `cp_itemid` varchar(10) DEFAULT NULL,
  `cp_itemprice` varchar(100) DEFAULT NULL,
  `cp_isactive` int(11) DEFAULT '0',
  `cp_updateddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_estimation`
--

CREATE TABLE `vm_estimation` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) NOT NULL,
  `be_vehicle_number` varchar(10) NOT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float DEFAULT NULL,
  `be_gtotal` varchar(100) NOT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) NOT NULL,
  `be_paydate` datetime NOT NULL,
  `be_balance` int(11) NOT NULL,
  `be_customerid` int(11) NOT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `be_oldbal` varchar(100) NOT NULL,
  `be_totvat` float NOT NULL,
  `be_debitid` varchar(100) NOT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) NOT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL,
  `finyear` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_estimation`
--

INSERT INTO `vm_estimation` (`be_billid`, `user_id`, `be_billnumber`, `be_customername`, `be_customermobile`, `be_customer_tin_num`, `be_vehicle_number`, `be_billdate`, `be_total`, `be_gtotal`, `be_paidamount`, `be_paymethod`, `be_note`, `be_updateddate`, `be_updatedby`, `be_isactive`, `be_discount`, `be_mode`, `be_paydate`, `be_balance`, `be_customerid`, `be_coolie`, `be_oldbal`, `be_totvat`, `be_debitid`, `be_creditid`, `be_statecode`, `finyear`) VALUES
(1, '1', 6, '', '', '', '', '2017-07-05 05:53:00', 200, '200', 200, NULL, NULL, '2017-07-05 05:54:10', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '', 0, '', '', ' ', 0),
(2, '1', 7, '', '', '', '', '2017-07-05 11:50:00', 550, '550', 550, NULL, NULL, '2017-07-05 11:51:12', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' ', 0),
(3, '1', 8, '', '', '', '', '2017-07-05 11:53:00', 2400, '2400', 2400, NULL, NULL, '2017-07-05 11:54:09', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' ', 0),
(4, '1', 9, 'das', '', '', '', '2017-07-10 13:30:00', 350, '350', 350, NULL, NULL, '2017-07-10 13:30:20', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' ', 0),
(5, '1', 10, 'das', '', '', '', '2017-07-10 13:32:00', 200, '200', 200, NULL, NULL, '2017-07-10 13:32:40', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_estimationitems`
--

CREATE TABLE `vm_estimationitems` (
  `bi_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `bi_billid` int(11) DEFAULT NULL,
  `bi_productid` int(11) DEFAULT NULL,
  `bi_price` float DEFAULT NULL,
  `bi_quantity` float DEFAULT NULL,
  `bi_discount` varchar(10) NOT NULL,
  `bi_total` float DEFAULT NULL,
  `bi_updatedon` datetime DEFAULT NULL,
  `bi_isactive` int(11) DEFAULT '0',
  `bi_vatamount` float DEFAULT NULL,
  `bi_vatper` float DEFAULT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `bi_sgst` float DEFAULT NULL,
  `bi_sgst_amt` float DEFAULT NULL,
  `bi_cgst` float DEFAULT NULL,
  `bi_cgst_amt` float DEFAULT NULL,
  `bi_igst` float DEFAULT NULL,
  `bi_igst_amt` float DEFAULT NULL,
  `finyear` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_estimationitems`
--

INSERT INTO `vm_estimationitems` (`bi_billitemid`, `user_id`, `bi_billid`, `bi_productid`, `bi_price`, `bi_quantity`, `bi_discount`, `bi_total`, `bi_updatedon`, `bi_isactive`, `bi_vatamount`, `bi_vatper`, `be_coolie`, `bi_sgst`, `bi_sgst_amt`, `bi_cgst`, `bi_cgst_amt`, `bi_igst`, `bi_igst_amt`, `finyear`) VALUES
(1, '1', 1, 1, 190.48, 1, '', 200, '2017-07-05 05:54:10', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0),
(2, '1', 2, 1, 190.48, 1, '', 200, '2017-07-05 11:51:12', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0),
(3, '1', 2, 3, 333.33, 1, '', 350, '2017-07-05 11:51:12', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0),
(4, '1', 3, 1, 190.48, 12, '0', 2400, '2017-07-05 11:54:09', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0),
(5, '1', 4, 3, 333.33, 1, '', 350, '2017-07-10 13:30:20', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0),
(6, '1', 5, 1, 190.48, 1, '', 200, '2017-07-10 13:32:40', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_financialyear`
--

CREATE TABLE `vm_financialyear` (
  `fy_id` int(11) NOT NULL,
  `fy_name` varchar(100) DEFAULT NULL,
  `fy_startdate` datetime DEFAULT NULL,
  `fy_enddate` datetime DEFAULT NULL,
  `fy_updatedtime` datetime DEFAULT NULL,
  `fy_default` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_godown`
--

CREATE TABLE `vm_godown` (
  `gd_id` int(11) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `gd_name` varchar(200) DEFAULT NULL,
  `gd_isactive` int(11) DEFAULT '0',
  `gd_updatedtime` datetime DEFAULT NULL,
  `gd_description` longtext,
  `gd_place` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_groupheads`
--

CREATE TABLE `vm_groupheads` (
  `gr_id` int(11) NOT NULL,
  `gr_head` varchar(110) DEFAULT NULL,
  `gr_type` varchar(110) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_groupheads`
--

INSERT INTO `vm_groupheads` (`gr_id`, `gr_head`, `gr_type`) VALUES
(1, 'SUNDRY CREDITORS', 'LIABILITY'),
(2, 'SUNDRY DEBTORS', 'ASSET'),
(3, 'CAPITAL', 'LIABILITY'),
(4, 'LOANS', 'LIABILITY'),
(7, 'CASH', 'ASSET'),
(8, 'BANK CURRENT ACCOUNT', 'ASSET'),
(9, 'SALE', 'ASSET'),
(10, 'PURCHASE', 'LIABILITY'),
(11, 'TAX AND DUTIES', 'ASSET'),
(12, 'DIRECT EXPENSE', 'LIABILITY'),
(13, 'INDIRECT EXPENSE', 'LIABILITY'),
(14, 'DIRECT INCOME', 'ASSET'),
(15, 'INDIRECT INCOME', 'ASSET'),
(16, 'CURRENT ASSETS', 'ASSET'),
(17, 'FIXED ASSETS', 'ASSET'),
(18, 'CURRENT LIABILITY', 'LIABILITY'),
(19, 'Salary1', '--Select--'),
(20, 'Tele expense', 'LIABILITY'),
(21, 'newlist', 'ASSET');

-- --------------------------------------------------------

--
-- Table structure for table `vm_note`
--

CREATE TABLE `vm_note` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `be_bill_id` int(11) DEFAULT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) DEFAULT NULL,
  `be_vehicle_number` varchar(10) DEFAULT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float(10,2) DEFAULT NULL,
  `be_gtotal` varchar(100) DEFAULT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) DEFAULT NULL,
  `be_paydate` datetime DEFAULT NULL,
  `be_balance` int(11) DEFAULT NULL,
  `be_customerid` int(11) DEFAULT NULL,
  `be_coolie` varchar(100) DEFAULT NULL,
  `be_oldbal` varchar(100) DEFAULT NULL,
  `be_totvat` float DEFAULT NULL,
  `be_debitid` varchar(100) DEFAULT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) DEFAULT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL,
  `be_brand` int(11) DEFAULT NULL,
  `be_reverse` varchar(100) DEFAULT NULL,
  `be_mode_pay` varchar(100) DEFAULT NULL,
  `be_duedate` datetime DEFAULT NULL,
  `be_reason` varchar(1000) DEFAULT NULL,
  `be_retunbillno` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_payment`
--

CREATE TABLE `vm_payment` (
  `pa_paymentid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pa_customerid` int(11) DEFAULT NULL,
  `pa_billid` int(11) DEFAULT NULL,
  `pa_balance` float DEFAULT NULL,
  `pa_newpayment` float DEFAULT NULL,
  `pa_newbalance` float DEFAULT NULL,
  `pa_isactive` int(11) DEFAULT '0',
  `pa_updatedon` datetime DEFAULT NULL,
  `pa_mode` varchar(10) DEFAULT NULL COMMENT '(1)customer(2)supplier',
  `pa_transactionid` varchar(100) DEFAULT NULL,
  `finyear` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_payment`
--

INSERT INTO `vm_payment` (`pa_paymentid`, `user_id`, `pa_customerid`, `pa_billid`, `pa_balance`, `pa_newpayment`, `pa_newbalance`, `pa_isactive`, `pa_updatedon`, `pa_mode`, `pa_transactionid`, `finyear`) VALUES
(1, 1, 4, NULL, 662, -8, 654, 0, '2020-03-13 15:52:07', '1', '9', NULL),
(2, 1, 4, NULL, 654, -14, 640, 0, '2020-03-13 15:54:13', '1', '10', NULL),
(3, 1, 1, NULL, 425, 10, 415, 0, '2020-03-16 14:05:05', '2', '11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vm_products`
--

CREATE TABLE `vm_products` (
  `pr_productid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pr_productcode` varchar(100) DEFAULT NULL,
  `pr_productname` varchar(1000) DEFAULT NULL,
  `pr_hsn` varchar(1000) DEFAULT NULL,
  `pr_purchaseprice` float DEFAULT NULL,
  `pr_saleprice` float DEFAULT NULL,
  `pr_description` longtext,
  `pr_stock` float DEFAULT NULL,
  `pr_isactive` int(11) DEFAULT '0',
  `pr_updateddate` datetime DEFAULT NULL,
  `pr_type` int(11) DEFAULT NULL,
  `pr_unit` varchar(700) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_products`
--

INSERT INTO `vm_products` (`pr_productid`, `user_id`, `pr_productcode`, `pr_productname`, `pr_hsn`, `pr_purchaseprice`, `pr_saleprice`, `pr_description`, `pr_stock`, `pr_isactive`, `pr_updateddate`, `pr_type`, `pr_unit`) VALUES
(1, '1', '1001', 'SAREE', '', 0, 0, NULL, 1877, 0, '2019-01-04 03:14:04', 4, 'Piece'),
(2, '1', '1002', 'BLOUSE BIT', '', 0, 0, '', 314, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(3, '1', '1003', 'SHIRTING', '', 0, 0, '', 2481.85, 0, '0000-00-00 00:00:00', 2, 'M'),
(4, '1', '1004', 'SUITING', '', 0, 0, '', 764.85, 0, '0000-00-00 00:00:00', 3, 'M'),
(5, '1', '1005', 'LINING', '', 0, 0, '', 2360, 0, '0000-00-00 00:00:00', 1, 'M'),
(6, '1', '1006', 'BLOUSE MATERIAL', '', 0, 0, '', 5957.64, 0, '0000-00-00 00:00:00', 1, 'M'),
(7, '1', '1007', 'CURTAIN CLOTH', '', 0, 0, '', 236.65, 0, '0000-00-00 00:00:00', 3, 'M'),
(8, '1', '1008', 'SET MUNDU', '', 0, 0, '', 28, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(9, '1', '1009', 'DHOTHI', '', 0, 0, '', 187, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(10, '1', '1010', 'LUNGI', '', 0, 0, '', 505, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(11, '1', '1011', 'SAREE', '', 0, 0, '', 1882, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(12, '1', '1012', 'BED SHEET', '', 0, 0, '', 126, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(13, '1', '1013', 'SHEETTING', '', 0, 0, '', 240, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(14, '1', '1014', 'MULL', '', 0, 0, '', 1416.5, 0, '0000-00-00 00:00:00', 1, 'M'),
(15, '1', '1015', 'SHIRT BIT', '', 0, 0, '', 97, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(16, '1', '1016', 'PATTU BIT', '', 0, 0, '', 87, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(17, '1', '1017', 'SATIN', '', 0, 0, '', 119, 0, '0000-00-00 00:00:00', 1, 'M'),
(18, '1', '1018', 'PANTIES', '', 0, 0, '', 2088, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(19, '1', '1019', 'BRA', '', 0, 0, '', 1383, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(20, '1', '1020', 'BANIAN', '', 0, 0, '', 182, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(21, '1', '1021', 'BRIEF', '', 0, 0, '', 1709, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(22, '1', '1022', 'SHIRT', '', 0, 0, '', 746, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(23, '1', '1023', 'FROCK', '', 0, 0, '', 918, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(24, '1', '1024', 'MIDDY TOP', '', 0, 0, '', 401, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(25, '1', '1025', 'CHURIDAR', '', 0, 0, '', 204, 0, '0000-00-00 00:00:00', 1, 'Piece'),
(26, '1', '1026', 'LEGGINGS', '', 0, 0, '', 78, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(27, '1', '1027', 'PATTIYALA', '', 0, 0, '', 79, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(28, '1', '1028', 'GIFT BOX', '', 0, 0, '', 5, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(29, '1', '1029', 'BABASUIT', '', 0, 0, '', 790, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(30, '1', '1030', 'NIGHTY', '', 0, 0, '', 447, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(31, '1', '1031', 'CARPET', '', 0, 0, '', 13, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(32, '1', '1032', 'SOMAN DHOTHI', '', 0, 0, '', 18, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(33, '1', '1033', 'PATTU ROLL', '', 0, 0, '', 100, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(34, '1', '1034', 'SIDE BAG', '', 0, 0, '', 19, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(35, '1', '1035', 'PATTU', '', 0, 0, '', 44, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(36, '1', '1036', 'T SHIRT', '', 0, 0, '', 50, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(37, '1', '1037', 'PATTA PATTI', '', 0, 0, '', 84, 0, '0000-00-00 00:00:00', 1, 'M'),
(38, '1', '1038', 'SHEET', '', 0, 0, '', 3, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(39, '1', '1039', 'NAMASS DRESS', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(40, '1', '1040', 'NIGHT DRESS', '', 0, 0, '', 21, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(41, '1', '1041', '3/4 SET', '', 0, 0, '', 25, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(42, '1', '1042', 'TRUNK PUNCH', '', 0, 0, '', 18, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(43, '1', '1043', 'PANT ', '', 0, 0, '', 498, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(44, '1', '1044', 'TABLESHEET', '', 0, 0, '', 26, 0, '0000-00-00 00:00:00', 3, 'Piece'),
(45, '1', '1045', 'BABY BED', '', 0, 0, '', 18, 0, '0000-00-00 00:00:00', 3, 'Piece'),
(46, '1', '1046', 'MOSQUITO NET', '', 0, 0, '', 82, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(47, '1', '1047', 'SOTTER', '', 0, 0, '', 59, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(48, '1', '1048', 'BERMUDA', '', 0, 0, '', 53, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(49, '1', '1049', 'SLIP', '', 0, 0, '', 9, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(50, '1', '1050', 'SKIRT', '', 0, 0, '', 315, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(51, '1', '1051', 'TWO - PIECE', '', 0, 0, '', 63, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(52, '1', '1052', 'TRACK SUIT', '', 0, 0, '', 31, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(53, '1', '1053', 'BABY SHEET', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(54, '1', '1054', 'CHURIDAR MATRL', '', 0, 0, '', 401, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(55, '1', '1055', 'THORTH', '', 0, 0, '', 1964, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(56, '1', '1056', 'PILLOW COVER', '', 0, 0, '', 88, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(57, '1', '1057', 'WESTERN', '', 0, 0, '', 182, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(58, '1', '1058', 'PATTU PAVADA', '', 0, 0, '', 27, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(59, '1', '1059', 'KAAVI MUNDE', '', 0, 0, '', 297, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(60, '1', '1060', 'BED COVER', '', 0, 0, '', 23, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(61, '1', '1061', 'POPLINE', '', 0, 0, '', 199, 0, '0000-00-00 00:00:00', 1, 'M'),
(62, '1', '1062', 'SINGLE DHOTHI', '', 0, 0, '', 215, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(63, '1', '1063', 'M DHOTHI', '', 0, 0, '', 7, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(64, '1', '1064', 'BEDDING CLOTH', '', 0, 0, '', 38.1, 0, '0000-00-00 00:00:00', 1, 'M'),
(65, '1', '1065', 'BLACK MUNDE', '', 0, 0, '', 65, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(66, '1', '1066', 'SALWAY', '', 0, 0, '', 46, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(67, '1', '1067', 'GADA', '', 0, 0, '', 680.7, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(68, '1', '1068', 'TOP', '', 0, 0, '', 358, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(69, '1', '1069', 'PATTU PAVADA BIT', '', 0, 0, '', 29, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(70, '1', '1070', 'CHAIR CANVAS', '', 0, 0, '', 13, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(71, '1', '1071', 'KIDS WEAR', '', 0, 0, '', 15, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(72, '1', '1072', 'MOSQUITO BED', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(73, '1', '1073', 'ERUMUDI', '', 0, 0, '', 5, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(74, '1', '1074', 'SKARF', '', 0, 0, '', 30, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(75, '1', '1075', 'DRAWER', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(76, '1', '1076', 'FALLS', '', 0, 0, '', 718, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(77, '1', '1077', 'COLOUR MULL', '', 0, 0, '', 318.4, 0, '0000-00-00 00:00:00', 1, 'M'),
(78, '1', '1078', 'BABY MAT', '', 0, 0, '', 10, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(79, '1', '1079', 'MIDDY', '', 0, 0, '', 4, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(80, '1', '1080', 'SHAWL', '', 0, 0, '', 24, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(81, '1', '1081', 'TURKKI', '', 0, 0, '', 7, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(82, '1', '1082', 'MOSQUITO NET BED', '', 885, 1100, '', 5, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(83, '1', '1083', 'BLANKET', '', 0, 0, '', 29, 0, '0000-00-00 00:00:00', 0, 'Piece');

-- --------------------------------------------------------

--
-- Table structure for table `vm_purentry`
--

CREATE TABLE `vm_purentry` (
  `pe_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pe_billnumber` int(11) DEFAULT NULL,
  `pe_customername` varchar(100) DEFAULT NULL,
  `pe_customermobile` int(11) DEFAULT NULL,
  `pe_billdate` datetime DEFAULT NULL,
  `pe_total` float DEFAULT NULL,
  `pe_gtotal` varchar(100) NOT NULL,
  `pe_oldbal` varchar(100) NOT NULL,
  `pe_paidamount` float DEFAULT NULL,
  `pe_paymethod` varchar(200) DEFAULT NULL,
  `pe_note` longtext,
  `pe_updateddate` datetime DEFAULT NULL,
  `pe_updatedby` varchar(250) DEFAULT NULL,
  `pe_isactive` int(11) DEFAULT '0',
  `pe_discount` float DEFAULT NULL,
  `pe_mode` varchar(100) DEFAULT NULL,
  `pe_paydate` datetime DEFAULT NULL,
  `pe_unitprice` int(100) NOT NULL,
  `pe_balance` float NOT NULL,
  `pe_supplierid` varchar(10) NOT NULL,
  `pe_vehicle_number` varchar(100) NOT NULL,
  `pe_invoice_number` varchar(100) NOT NULL,
  `pe_invoice_date` date NOT NULL,
  `pe_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_purentry`
--

INSERT INTO `vm_purentry` (`pe_billid`, `user_id`, `pe_billnumber`, `pe_customername`, `pe_customermobile`, `pe_billdate`, `pe_total`, `pe_gtotal`, `pe_oldbal`, `pe_paidamount`, `pe_paymethod`, `pe_note`, `pe_updateddate`, `pe_updatedby`, `pe_isactive`, `pe_discount`, `pe_mode`, `pe_paydate`, `pe_unitprice`, `pe_balance`, `pe_supplierid`, `pe_vehicle_number`, `pe_invoice_number`, `pe_invoice_date`, `pe_statecode`) VALUES
(1, 1, 1, '', 0, '2020-01-09 15:05:00', 1900, '1900', '0', 1900, NULL, NULL, '2020-01-09 15:07:07', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '', 'KL08HJ3434', 'dssfs93849342', '2020-01-09', ''),
(2, 1, 2, '', 0, '2020-03-16 14:00:00', 2350, '3925', '1575', 3500, NULL, NULL, '2020-03-16 14:02:09', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 425, '1', 'KL04YT7676', 'dsfdt43543534', '2020-03-16', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_puritems`
--

CREATE TABLE `vm_puritems` (
  `pi_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pi_billid` int(11) DEFAULT NULL,
  `pi_productid` int(11) DEFAULT NULL,
  `pi_price` float DEFAULT NULL,
  `pi_quantity` float DEFAULT NULL,
  `pi_total` float DEFAULT NULL,
  `pi_updatedon` datetime DEFAULT NULL,
  `pi_isactive` int(11) DEFAULT '0',
  `pi_vatamount` float DEFAULT NULL,
  `pi_vatper` float DEFAULT NULL,
  `pi_unitprice` int(100) NOT NULL,
  `pi_sgst` float DEFAULT NULL,
  `pi_sgstamt` float DEFAULT NULL,
  `pi_cgst` float DEFAULT NULL,
  `pi_cgstamt` float DEFAULT NULL,
  `pi_igst` float DEFAULT NULL,
  `pi_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_puritems`
--

INSERT INTO `vm_puritems` (`pi_billitemid`, `user_id`, `pi_billid`, `pi_productid`, `pi_price`, `pi_quantity`, `pi_total`, `pi_updatedon`, `pi_isactive`, `pi_vatamount`, `pi_vatper`, `pi_unitprice`, `pi_sgst`, `pi_sgstamt`, `pi_cgst`, `pi_cgstamt`, `pi_igst`, `pi_igstamt`) VALUES
(1, 1, 1, 3, 1056, 1, 1200, '2020-01-09 15:07:07', 0, 144, 12, 0, 0, 0, 0, 0, 12, 144),
(2, 1, 1, 4, 369, 1, 450, '2020-01-09 15:07:07', 0, 81, 18, 0, 0, 0, 0, 0, 18, 81),
(3, 1, 1, 9, 250, 1, 250, '2020-01-09 15:07:07', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 1, 2, 5, 1187.5, 1, 1250, '2020-03-16 14:02:09', 0, 62.5, 5, 0, 2.5, 31.25, 2.5, 31.25, 0, 0),
(5, 1, 2, 7, 902, 1, 1100, '2020-03-16 14:02:09', 0, 198, 18, 0, 9, 99, 9, 99, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_purorder`
--

CREATE TABLE `vm_purorder` (
  `pe_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pe_billnumber` int(11) DEFAULT NULL,
  `pe_customername` varchar(100) DEFAULT NULL,
  `pe_customermobile` int(11) DEFAULT NULL,
  `pe_billdate` datetime DEFAULT NULL,
  `pe_total` float DEFAULT NULL,
  `pe_gtotal` varchar(100) NOT NULL,
  `pe_oldbal` varchar(100) NOT NULL,
  `pe_paidamount` float DEFAULT NULL,
  `pe_paymethod` varchar(200) DEFAULT NULL,
  `pe_note` longtext,
  `pe_updateddate` datetime DEFAULT NULL,
  `pe_updatedby` varchar(250) DEFAULT NULL,
  `pe_isactive` int(11) DEFAULT '0',
  `pe_discount` float DEFAULT NULL,
  `pe_mode` varchar(100) DEFAULT NULL,
  `pe_paydate` datetime DEFAULT NULL,
  `pe_unitprice` int(100) NOT NULL,
  `pe_balance` float NOT NULL,
  `pe_supplierid` varchar(10) NOT NULL,
  `pe_vehicle_number` varchar(100) NOT NULL,
  `pe_invoice_number` varchar(100) NOT NULL,
  `pe_invoice_date` date NOT NULL,
  `pe_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purorderitems`
--

CREATE TABLE `vm_purorderitems` (
  `pi_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pi_billid` int(11) DEFAULT NULL,
  `pi_productid` int(11) DEFAULT NULL,
  `pi_price` float DEFAULT NULL,
  `pi_quantity` float DEFAULT NULL,
  `pi_total` float DEFAULT NULL,
  `pi_updatedon` datetime DEFAULT NULL,
  `pi_isactive` int(11) DEFAULT '0',
  `pi_vatamount` float DEFAULT NULL,
  `pi_vatper` float DEFAULT NULL,
  `pi_unitprice` int(100) NOT NULL,
  `pi_sgst` float DEFAULT NULL,
  `pi_sgstamt` float DEFAULT NULL,
  `pi_cgst` float DEFAULT NULL,
  `pi_cgstamt` float DEFAULT NULL,
  `pi_igst` float DEFAULT NULL,
  `pi_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purreturnentry`
--

CREATE TABLE `vm_purreturnentry` (
  `pre_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pre_billnumber` int(11) DEFAULT NULL,
  `pre_customername` varchar(600) DEFAULT NULL,
  `pre_customermobile` varchar(20) DEFAULT NULL,
  `pre_customer_tin_num` varchar(100) NOT NULL,
  `pre_vehicle_number` varchar(10) NOT NULL,
  `pre_billdate` datetime DEFAULT NULL,
  `pre_total` float DEFAULT NULL,
  `pre_gtotal` float NOT NULL,
  `pre_paidamount` float DEFAULT NULL,
  `pre_paymethod` varchar(250) DEFAULT NULL,
  `pre_note` longtext,
  `pre_updateddate` datetime DEFAULT NULL,
  `pre_updatedby` varchar(250) DEFAULT NULL,
  `pre_isactive` int(11) DEFAULT '0',
  `pre_discount` float DEFAULT NULL,
  `pre_mode` varchar(100) NOT NULL,
  `pre_paydate` datetime NOT NULL,
  `pre_balance` int(11) NOT NULL,
  `pre_customerid` int(11) NOT NULL,
  `pre_coolie` varchar(100) NOT NULL,
  `pre_oldbal` varchar(100) NOT NULL,
  `pre_totvat` float DEFAULT NULL,
  `pre_invoice_number` varchar(100) NOT NULL,
  `pre_invoice_date` date NOT NULL,
  `pre_rebill` varchar(100) DEFAULT NULL,
  `pre_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purreturnitem`
--

CREATE TABLE `vm_purreturnitem` (
  `pri_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pri_billid` int(11) NOT NULL,
  `pri_returnbillid` int(11) NOT NULL,
  `pri_productid` int(11) NOT NULL,
  `pri_price` varchar(100) NOT NULL,
  `pri_quantity` float NOT NULL,
  `pri_total` float NOT NULL,
  `pri_updatedon` datetime NOT NULL,
  `pri_isactive` int(11) NOT NULL,
  `pri_vatamount` float NOT NULL,
  `pri_vatper` float NOT NULL,
  `pri_coolie` float NOT NULL,
  `pri_sgst` float DEFAULT NULL,
  `pri_sgstamt` float DEFAULT NULL,
  `pri_cgst` float DEFAULT NULL,
  `pr_cgstamt` float DEFAULT NULL,
  `pri_igst` float DEFAULT NULL,
  `pri_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_salreturnentry`
--

CREATE TABLE `vm_salreturnentry` (
  `sre_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sre_billnumber` int(11) DEFAULT NULL,
  `sre_customername` varchar(100) DEFAULT NULL,
  `sre_customeraddress` varchar(200) DEFAULT NULL,
  `sre_customermobile` varchar(100) DEFAULT NULL,
  `sre_customer_tin_num` varchar(100) NOT NULL,
  `sre_billdate` datetime DEFAULT NULL,
  `sre_total` float DEFAULT NULL,
  `sre_gtotal` varchar(100) NOT NULL,
  `sre_oldbal` varchar(100) NOT NULL,
  `sre_paidamount` float DEFAULT NULL,
  `sre_paymethod` varchar(200) DEFAULT NULL,
  `sre_note` longtext,
  `sre_updateddate` datetime DEFAULT NULL,
  `sre_updatedby` varchar(250) DEFAULT NULL,
  `sre_isactive` int(11) DEFAULT '0',
  `sre_discount` float DEFAULT NULL,
  `sre_mode` varchar(100) DEFAULT NULL,
  `sre_paydate` datetime DEFAULT NULL,
  `sre_unitprice` int(100) NOT NULL,
  `sre_balance` float NOT NULL,
  `sre_supplierid` int(10) DEFAULT NULL,
  `sre_vehicle_number` varchar(100) NOT NULL,
  `sre_rebill` varchar(100) DEFAULT NULL,
  `sre_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_salreturnitem`
--

CREATE TABLE `vm_salreturnitem` (
  `sri_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sri_billid` int(11) NOT NULL,
  `sri_returnbillid` int(11) NOT NULL,
  `sri_productid` int(11) NOT NULL,
  `sri_price` float NOT NULL,
  `sri_quantity` float NOT NULL,
  `sri_total` float NOT NULL,
  `sri_updatedon` datetime NOT NULL,
  `sri_isactive` int(11) NOT NULL,
  `sri_vatamount` float NOT NULL,
  `sri_vatper` float NOT NULL,
  `sri_unitprice` float NOT NULL,
  `sri_sgst` float DEFAULT NULL,
  `sri_sgstamt` float DEFAULT NULL,
  `sri_cgst` float DEFAULT NULL,
  `sri_cgstamt` float DEFAULT NULL,
  `sri_igst` float DEFAULT NULL,
  `sri_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_shopprofile`
--

CREATE TABLE `vm_shopprofile` (
  `sp_shopid` int(11) NOT NULL,
  `sp_shopname` varchar(600) DEFAULT NULL,
  `sp_shopaddress` varchar(1000) DEFAULT NULL,
  `sp_phone` varchar(20) DEFAULT NULL,
  `sp_mobile` varchar(20) DEFAULT NULL,
  `sp_email` varchar(350) DEFAULT NULL,
  `sp_logo` varchar(1000) DEFAULT NULL,
  `sp_username` varchar(50) DEFAULT NULL,
  `sp_password` varchar(50) DEFAULT NULL,
  `sp_isactive` int(11) DEFAULT '0',
  `sp_vatreadymades` float DEFAULT NULL,
  `sp_vatmillgoods` float DEFAULT NULL,
  `sp_tin` varchar(100) NOT NULL,
  `sp_cst` varchar(200) NOT NULL,
  `sp_adddate` date NOT NULL,
  `sp_acnttype` int(11) NOT NULL DEFAULT '0' COMMENT '(0)trail(1)payed',
  `sp_trlprd` int(11) NOT NULL,
  `sp_barcode` varchar(10) NOT NULL,
  `sp_stcode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_shopprofile`
--

INSERT INTO `vm_shopprofile` (`sp_shopid`, `sp_shopname`, `sp_shopaddress`, `sp_phone`, `sp_mobile`, `sp_email`, `sp_logo`, `sp_username`, `sp_password`, `sp_isactive`, `sp_vatreadymades`, `sp_vatmillgoods`, `sp_tin`, `sp_cst`, `sp_adddate`, `sp_acnttype`, `sp_trlprd`, `sp_barcode`, `sp_stcode`) VALUES
(1, 'AKBAR CLOTH STORE', 'Main Road, Koduvayur', '9876543210', '', 'akbar@mail.com', NULL, 'admin', 'admin123', 0, 0, 0, '123456', '', '2019-01-07', 1, 3, 'US-ebiller', 'KL'),
(2, 'uesr1', '', '', '', '', NULL, 'new', '1234', 1, NULL, NULL, '', '', '2019-07-07', 0, 10, 'user', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_stockreport`
--

CREATE TABLE `vm_stockreport` (
  `sr_itemid` int(11) NOT NULL,
  `sr_date` datetime NOT NULL,
  `sr_qty` varchar(100) NOT NULL,
  `sr_amount` varchar(100) NOT NULL,
  `sr_closingstock` varchar(100) NOT NULL,
  `sr_mode` varchar(100) NOT NULL,
  `finyear` int(11) NOT NULL,
  `sr_id` int(11) NOT NULL,
  `billid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_supplier`
--

CREATE TABLE `vm_supplier` (
  `rs_supplierid` int(11) NOT NULL,
  `rs_company_name` varchar(100) NOT NULL,
  `rs_name` varchar(50) DEFAULT NULL,
  `rs_phone` varchar(20) DEFAULT NULL,
  `rs_mobile` varchar(20) DEFAULT NULL,
  `rs_address` longtext,
  `rs_email` varchar(100) NOT NULL,
  `rs_balance` varchar(10) NOT NULL,
  `rs_isactive` int(11) DEFAULT '0',
  `rs_tinnum` varchar(100) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `rs_acntid` varchar(100) NOT NULL,
  `rs_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_supplier`
--

INSERT INTO `vm_supplier` (`rs_supplierid`, `rs_company_name`, `rs_name`, `rs_phone`, `rs_mobile`, `rs_address`, `rs_email`, `rs_balance`, `rs_isactive`, `rs_tinnum`, `user_id`, `rs_acntid`, `rs_statecode`) VALUES
(1, 'Saj exporters', 'Sajan', '9043940304', NULL, 'new address, new street, new location', 'sajan@mail.com', '415', 0, '3ewrfs4354', '1', '', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_transaction`
--

CREATE TABLE `vm_transaction` (
  `tr_id` int(11) NOT NULL,
  `tr_billid` int(11) DEFAULT NULL,
  `tr_particulars` varchar(500) DEFAULT NULL,
  `tr_openingbalance` float DEFAULT NULL,
  `tr_transactionamount` float DEFAULT NULL,
  `tr_closingbalance` float DEFAULT NULL,
  `tr_date` datetime DEFAULT NULL,
  `tr_transactiontype` varchar(500) DEFAULT NULL,
  `tr_isactive` int(11) DEFAULT '0',
  `tr_updateddate` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tr_mode` int(10) DEFAULT '0',
  `pay_type` varchar(100) DEFAULT NULL,
  `cus_id` varchar(100) DEFAULT NULL,
  `tr_name` varchar(100) DEFAULT NULL,
  `tr_accid` int(100) DEFAULT NULL,
  `finyear` int(100) DEFAULT NULL,
  `rpt_no` varchar(100) NOT NULL,
  `rpt_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_transaction`
--

INSERT INTO `vm_transaction` (`tr_id`, `tr_billid`, `tr_particulars`, `tr_openingbalance`, `tr_transactionamount`, `tr_closingbalance`, `tr_date`, `tr_transactiontype`, `tr_isactive`, `tr_updateddate`, `user_id`, `tr_mode`, `pay_type`, `cus_id`, `tr_name`, `tr_accid`, `finyear`, `rpt_no`, `rpt_status`) VALUES
(1, 1, 'Sales', 0, 190, 190, '2019-01-21 12:06:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(2, 1, 'Purchase', 190, 172.48, 17.52, '2019-01-21 12:06:00', 'expense', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(3, 2, 'Sales', 17.52, 452, 469.52, '2019-01-24 16:08:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(4, 1, 'Purchase', 469.52, 764, -294.48, '2019-02-02 10:52:00', 'expense', 0, NULL, 2, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(5, 3, 'Sales', -294.48, 359, 64.52, '2019-02-04 15:00:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(6, 4, 'Sales', 64.52, 375, 439.52, '2019-02-04 15:30:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(7, 5, 'Sales', 439.52, 1102, 1541.52, '2019-02-04 15:32:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(8, 6, 'Sales', 1541.52, 328, 1869.52, '2019-02-04 17:22:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(9, 7, 'Sales', 1869.52, 467, 2336.52, '2019-02-04 17:23:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(10, 8, 'Sales', 2336.52, 222, 2558.52, '2019-02-04 17:24:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(11, 9, 'Sales', 2558.52, 455, 3013.52, '2019-02-04 17:26:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(12, 10, 'Sales', 3013.52, 34, 3047.52, '2019-02-13 13:25:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(13, 11, 'Sales', 3047.52, 35, 3082.52, '2019-02-13 14:35:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 4, '', ''),
(14, 12, 'Sales', 3082.52, 654, 3736.52, '2019-02-23 21:53:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(15, 13, 'Sales', 3736.52, 300, 4036.52, '2019-08-02 19:55:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(16, 14, 'Sales', 4036.52, 180, 4216.52, '2019-08-31 13:42:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(17, 15, 'Sales', 4216.52, 200, 4416.52, '2019-11-15 11:57:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(18, 16, 'Sales', 4416.52, 222, 4638.52, '2019-11-25 11:11:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(19, 17, 'Sales', 4638.52, 254, 4892.52, '2019-11-25 12:50:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(20, 18, 'Sales', 4892.52, 145, 5037.52, '2019-11-25 13:37:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, 0, '', ''),
(21, NULL, '', 5037.52, 12, 5049.52, '2020-03-10 01:48:58', 'Customer Balance Payment - Receipt No:1 ', 0, NULL, 1, 1, NULL, NULL, '1', 1, 1, '1', 'r'),
(22, NULL, '', 5049.52, 2, 5051.52, '2020-03-11 16:08:37', 'Customer Balance Payment - Receipt No:2 ', 0, NULL, 1, 1, NULL, NULL, '1', 2, 0, '2', 'r'),
(23, 2, 'Sales', 5051.52, 3700, 8751.52, '2020-03-11 16:09:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(24, 3, 'Sales', 8751.52, 5000, 13751.5, '2020-03-11 16:11:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(25, NULL, '', 13751.5, 700, 14451.5, '2020-03-11 16:13:42', 'Customer Balance Payment - Receipt No:3 ', 0, NULL, 1, 1, NULL, NULL, '1', 3, 0, '3', 'r'),
(26, NULL, '', 14451.5, 20, 14471.5, '2020-03-12 21:36:43', 'Customer Balance Payment - Receipt No:4 ', 0, NULL, 1, 1, NULL, NULL, '1', 4, 0, '4', 'r'),
(27, NULL, '', 14471.5, 4, 14475.5, '2020-03-12 21:37:46', 'Customer Balance Payment - Receipt No:5 ', 0, NULL, 1, 1, NULL, NULL, '1', 5, 0, '5', 'r'),
(28, NULL, '47', 14475.5, 12, 14487.5, '2020-03-13 08:07:39', 'Customer Balance Payment - Receipt No:6 ', 0, NULL, 1, 1, NULL, NULL, '1', 7, 0, '6', 'r'),
(29, NULL, '47', 14487.5, 6, 14493.5, '2020-03-13 14:07:04', 'Customer Balance Payment - Receipt No:7 ', 0, NULL, 1, 1, NULL, NULL, '1', 8, 0, '7', 'r'),
(30, NULL, '47', 14493.5, 8, 14501.5, '2020-03-13 15:52:07', 'Customer Balance Payment - Receipt No:8 ', 0, NULL, 1, 1, NULL, NULL, '1', 9, 0, '8', 'r'),
(31, NULL, '47', 14501.5, 14, 14515.5, '2020-03-13 15:54:13', 'Customer Balance Payment - Receipt No:9 ', 0, NULL, 1, 1, NULL, NULL, '1', 10, 0, '9', 'r'),
(32, 4, 'Sales', 14515.5, 4210, 18725.5, '2020-03-13 16:31:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(33, 5, 'Sales', 18725.5, 4390, 23115.5, '2020-03-13 17:34:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(34, 2, 'Purchase', 23115.5, 3500, 19615.5, '2020-03-16 14:00:00', 'expense', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(35, NULL, '', 19615.5, 10, 19605.5, '2020-03-16 14:05:05', 'Supplier Balance Payment - Voucher No:1', 0, NULL, 1, 1, NULL, NULL, '1', 11, 0, '1', 'v'),
(36, 6, 'Sales', 19605.5, 2698, 22303.5, '2020-03-17 20:40:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(37, 7, 'Sales', 22303.5, 5100, 27403.5, '2020-03-17 20:42:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(38, 8, 'Sales', 27403.5, 3230, 30633.5, '2020-03-18 10:31:00', 'income', 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator_account_name`
--
ALTER TABLE `administrator_account_name`
  ADD PRIMARY KEY (`refid`);

--
-- Indexes for table `administrator_daybook`
--
ALTER TABLE `administrator_daybook`
  ADD PRIMARY KEY (`refid`);

--
-- Indexes for table `vm_billentry`
--
ALTER TABLE `vm_billentry`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_billitems`
--
ALTER TABLE `vm_billitems`
  ADD PRIMARY KEY (`bi_billitemid`);

--
-- Indexes for table `vm_catogory`
--
ALTER TABLE `vm_catogory`
  ADD PRIMARY KEY (`ca_categoryid`);

--
-- Indexes for table `vm_customer`
--
ALTER TABLE `vm_customer`
  ADD PRIMARY KEY (`cs_customerid`);

--
-- Indexes for table `vm_customeritemprice`
--
ALTER TABLE `vm_customeritemprice`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `vm_estimation`
--
ALTER TABLE `vm_estimation`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_estimationitems`
--
ALTER TABLE `vm_estimationitems`
  ADD PRIMARY KEY (`bi_billitemid`);

--
-- Indexes for table `vm_financialyear`
--
ALTER TABLE `vm_financialyear`
  ADD PRIMARY KEY (`fy_id`);

--
-- Indexes for table `vm_godown`
--
ALTER TABLE `vm_godown`
  ADD PRIMARY KEY (`gd_id`);

--
-- Indexes for table `vm_groupheads`
--
ALTER TABLE `vm_groupheads`
  ADD PRIMARY KEY (`gr_id`);

--
-- Indexes for table `vm_note`
--
ALTER TABLE `vm_note`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_payment`
--
ALTER TABLE `vm_payment`
  ADD PRIMARY KEY (`pa_paymentid`);

--
-- Indexes for table `vm_products`
--
ALTER TABLE `vm_products`
  ADD PRIMARY KEY (`pr_productid`);

--
-- Indexes for table `vm_purentry`
--
ALTER TABLE `vm_purentry`
  ADD PRIMARY KEY (`pe_billid`);

--
-- Indexes for table `vm_puritems`
--
ALTER TABLE `vm_puritems`
  ADD PRIMARY KEY (`pi_billitemid`);

--
-- Indexes for table `vm_purorder`
--
ALTER TABLE `vm_purorder`
  ADD PRIMARY KEY (`pe_billid`);

--
-- Indexes for table `vm_purorderitems`
--
ALTER TABLE `vm_purorderitems`
  ADD PRIMARY KEY (`pi_billitemid`);

--
-- Indexes for table `vm_purreturnentry`
--
ALTER TABLE `vm_purreturnentry`
  ADD PRIMARY KEY (`pre_billid`);

--
-- Indexes for table `vm_purreturnitem`
--
ALTER TABLE `vm_purreturnitem`
  ADD PRIMARY KEY (`pri_billitemid`);

--
-- Indexes for table `vm_salreturnentry`
--
ALTER TABLE `vm_salreturnentry`
  ADD PRIMARY KEY (`sre_billid`);

--
-- Indexes for table `vm_salreturnitem`
--
ALTER TABLE `vm_salreturnitem`
  ADD PRIMARY KEY (`sri_billitemid`);

--
-- Indexes for table `vm_shopprofile`
--
ALTER TABLE `vm_shopprofile`
  ADD PRIMARY KEY (`sp_shopid`);

--
-- Indexes for table `vm_stockreport`
--
ALTER TABLE `vm_stockreport`
  ADD PRIMARY KEY (`sr_itemid`);

--
-- Indexes for table `vm_supplier`
--
ALTER TABLE `vm_supplier`
  ADD PRIMARY KEY (`rs_supplierid`);

--
-- Indexes for table `vm_transaction`
--
ALTER TABLE `vm_transaction`
  ADD PRIMARY KEY (`tr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator_account_name`
--
ALTER TABLE `administrator_account_name`
  MODIFY `refid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `administrator_daybook`
--
ALTER TABLE `administrator_daybook`
  MODIFY `refid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vm_billentry`
--
ALTER TABLE `vm_billentry`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vm_billitems`
--
ALTER TABLE `vm_billitems`
  MODIFY `bi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `vm_catogory`
--
ALTER TABLE `vm_catogory`
  MODIFY `ca_categoryid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vm_customer`
--
ALTER TABLE `vm_customer`
  MODIFY `cs_customerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vm_customeritemprice`
--
ALTER TABLE `vm_customeritemprice`
  MODIFY `cp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_estimation`
--
ALTER TABLE `vm_estimation`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vm_estimationitems`
--
ALTER TABLE `vm_estimationitems`
  MODIFY `bi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vm_financialyear`
--
ALTER TABLE `vm_financialyear`
  MODIFY `fy_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_godown`
--
ALTER TABLE `vm_godown`
  MODIFY `gd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_groupheads`
--
ALTER TABLE `vm_groupheads`
  MODIFY `gr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `vm_note`
--
ALTER TABLE `vm_note`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_payment`
--
ALTER TABLE `vm_payment`
  MODIFY `pa_paymentid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vm_products`
--
ALTER TABLE `vm_products`
  MODIFY `pr_productid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `vm_purentry`
--
ALTER TABLE `vm_purentry`
  MODIFY `pe_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vm_puritems`
--
ALTER TABLE `vm_puritems`
  MODIFY `pi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vm_purorder`
--
ALTER TABLE `vm_purorder`
  MODIFY `pe_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purorderitems`
--
ALTER TABLE `vm_purorderitems`
  MODIFY `pi_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purreturnentry`
--
ALTER TABLE `vm_purreturnentry`
  MODIFY `pre_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purreturnitem`
--
ALTER TABLE `vm_purreturnitem`
  MODIFY `pri_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_salreturnentry`
--
ALTER TABLE `vm_salreturnentry`
  MODIFY `sre_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_salreturnitem`
--
ALTER TABLE `vm_salreturnitem`
  MODIFY `sri_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_shopprofile`
--
ALTER TABLE `vm_shopprofile`
  MODIFY `sp_shopid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vm_stockreport`
--
ALTER TABLE `vm_stockreport`
  MODIFY `sr_itemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_supplier`
--
ALTER TABLE `vm_supplier`
  MODIFY `rs_supplierid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vm_transaction`
--
ALTER TABLE `vm_transaction`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
