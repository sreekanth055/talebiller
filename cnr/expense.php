<?php
session_start();
if(isset($_SESSION['admin']))
{
	include("includes/config.php");
	if(isset($_POST['submit']))
	{
		$particulars=$_POST['particulars'];
		$transactiontype=$_POST['type'];
		$date=$_POST['date'];
		$time=$_POST['time'];
		$datetime=date("Y-m-d", strtotime($_POST["date"]))." ".date("H:i", strtotime($_POST["time"]));
		$amount=$_POST['amount'];
		
 $slct_empnum=$conn->query("SELECT	tr_closingbalance FROM vm_transaction ORDER BY tr_id DESC LIMIT 1");
if(mysqli_num_rows($slct_empnum)>0)
 {
$last=$slct_empnum->fetch_assoc();
$openingbalance = $last['tr_closingbalance'] ;
 }
 else{
 $openingbalance =0;
 }

	if($transactiontype=='expense')	
	{
		$closingbalance=$openingbalance-$amount;
	}
	elseif($transactiontype=='income')
	{
		$closingbalance=$openingbalance+$amount;
	}
	
		
		
$insert=$conn->query("insert into vm_transaction(tr_particulars,tr_openingbalance,tr_transactionamount,tr_closingbalance,tr_date,tr_transactiontype,user_id)
values('$particulars',' $openingbalance','$amount','$closingbalance','$datetime','$transactiontype','".$_SESSION["admin"]."')");

if($insert)
	{header("location:expense.php?id");}
else{header("location:expense.php?er");}
  

		
	}
?>
<!DOCTYPE html>
<html>  
<head>  
        <!-- Title -->
         <title> Tale Biller </title>
        
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="Administrator" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="administrator" />
        
         <style>
	#results td:hover{
		background-color:rgba(58, 87, 149, 0.28);
		
	}
	.secol table td{
cursor:pointer;
padding:3px;
}
.secol table td:hover{
background-color:rgba(58, 87, 149, 0.39);
}
	
	</style>
    <!-- Styles -->              
        <link href="assets/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/> 
        <link href="assets/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/> 
        <link href="assets/plugins/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" type="text/css">
        <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/> 
        <!-- Theme Styles -->
        
</head>
<body class="page-header-fixed">
    
<div class="overlay"></div>   
        <main class="page-content content-wrap">
            <?php
			include("header.php");
			?>
            <!-- Navbar -->

            <div class="page-inner">
                <div class="page-title">    
                    <h3><strong style="color:#6699cc;">Expense</strong></h3>

              <div class="template-demo">
                <nav aria-label="breadcrumb" role="navigation">
                  <ol class="breadcrumb breadcrumb-custom">
                       <li class="breadcrumb-item"><a href="#">Dashboard </a> </li>
                    <li class="breadcrumb-item active">Expense </li>
                  </ol>
                </nav><div class="pull-right" style="color:#145252;"> <u>F7</u> - Home </div>
              </div>


                <div id="main-wrapper">
                    <!-- Row -->
                    <div class="row">

                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-white">
							
                                <?php
								if(isset($_GET['id']))
								{
									?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        Saved successfully.
                                    </div>
                                    <?php
								}
								if(isset($_GET['er']))
								{
									
									?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
										
                                        Error occured, please try again.
                                    </div>
                                    <?php
								}
								?>
                                <div class="panel-body">
                                    <div class="project-stats">  
                                    
                                   <form class="form-horizontal" name="addbilldetails" method="post" action="" enctype="multipart/form-data">
                                       <div class="table-responsive">
                                       <input type="hidden" name="billno" id="billno" value="">   
                                   <div class="table-responsive">                                        
                                    <table class="table" id="drgcartitms">
                                    <thead>
                                    <tr>
									<th>Particulars</th>
									<th>Type</th>
									<th>Date</th>
									<th>Time</th>
									<th>Amount</th>
                                    </tr>
                                    </thead>
									<tbody>
									<tr>
									<td><input type="text" required class="form-control" placeholder="Particulars" name="particulars" id="particulars"></td>
                                    <td style="width:124px;"><select class="form-control" name="type">
                                 <option value="expense">Expense</option>
                                 <option value="income">Income</option>
                                    </select>
									</td>
									<td> <input type="date" name="date" id="date" class="form-control"></td>
									<td><input type="time" name="time" id="time" value="<?= date('H:i') ?>" class="form-control"></td></td>
									<td><input type="text" required  class="form-control" placeholder="Amount" name="amount" id="amout"></td>
									
                                        </tr>                           
									</tbody>
								    </table>
                                   </div> 
                                    
                                        <div class="form-group" align="right" style="padding-right:30px;">
                                        <label for="input-help-block" class="col-sm-2 control-label"></label>
                                        <button type="submit" name="submit" class="btn btn-primary">Save</button>
                                        </div>
										</div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					</div>
								

            </div><!-- Page Inner -->
        </main><!-- Page Content -->
        
        <div class="cd-overlay"></div>

        <?php
            include("footer.php");
        ?>

         <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/plugins/pace-master/pace.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/offcanvasmenueffects/js/classie.js"></script>
        <script src="assets/plugins/offcanvasmenueffects/js/main.js"></script>
        <script src="assets/plugins/waves/waves.min.js"></script>
        <script src="assets/plugins/3d-bold-navigation/js/main.js"></script>
        <script src="assets/plugins/jquery-mockjax-master/jquery.mockjax.js"></script>
        <script src="assets/plugins/moment/moment.js"></script>
        <script src="assets/plugins/datatables/js/jquery.datatables.min.js"></script>
        <script src="assets/plugins/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/js/modern.min.js"></script>
        <script src="assets/js/pages/table-data.js"></script>

	</body>

</html>
<?php
}else{
	header("Location:index.php");
}
?>