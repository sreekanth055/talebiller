<?php
error_reporting(0);
session_start();
$username = $_SESSION['user'];
$shopname = $_SESSION['name'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title> Tale Biller </title>
  <!-- plugins:css -->
      <link href="assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>  
  <link rel="stylesheet" href="../assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../assets/vendors/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="../assets/vendors/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.base.css">
<!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>-->
        
  <link href="assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>  
  <link href="assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>  
  <!-- endinject -->

  <!-- inject:css -->
  <link rel="stylesheet" href="../assets/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="assets/images/favicon.png" />

<style>
  #results td:hover{
    background-color:rgba(58, 87, 149, 0.28);
  }

.secol table td{
cursor:pointer;
padding:3px;
}

.secol table td:hover{
background-color:rgba(58, 87, 149, 0.39);
}

.site-footer
{
  background-color:#26272b;
  padding:45px 0 20px;
  font-size:15px;
  line-height:24px;
  color:#737373;
}

#footer {
 position: relative;
   clear: both;
 bottom: 0;
 width: 100%;
}
.site-footer hr
{
  border-top-color:#bbb;
  opacity:0.5
}
.site-footer hr.small
{
  margin:20px 0
}
.site-footer h6
{
  color:#fff;
  font-size:16px;
  text-transform:uppercase;
  margin-top:5px;
  letter-spacing:2px
}
.site-footer a
{
  color:#737373;
}
.site-footer a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links
{
  padding-left:0;
  list-style:none
}
.footer-links li
{
  display:block
}
.footer-links a
{
  color:#737373
}
.footer-links a:active,.footer-links a:focus,.footer-links a:hover
{
  color:#3366cc;
  text-decoration:none;
}
.footer-links.inline li
{
  display:inline-block
}
.site-footer .social-icons
{
  text-align:right
}
.site-footer .social-icons a
{
  width:40px;
  height:40px;
  line-height:40px;
  margin-left:6px;
  margin-right:0;
  border-radius:100%;
  background-color:#33353d
}
.copyright-text
{
  margin:0
}
@media (max-width:991px)
{
  .site-footer [class^=col-]
  {
    margin-bottom:30px
  }
}
@media (max-width:767px)
{
  .site-footer
  {
    padding-bottom:0
  }
  .site-footer .copyright-text,.site-footer .social-icons
  {
    text-align:center
  }
}
.social-icons
{
  padding-left:0;
  margin-bottom:0;
  list-style:none
}
.social-icons li
{
  display:inline-block;
  margin-bottom:4px
}
.social-icons li.title
{
  margin-right:15px;
  text-transform:uppercase;
  color:#96a2b2;
  font-weight:700;
  font-size:13px
}
.social-icons a{
  background-color:#eceeef;
  color:#818a91;
  font-size:16px;
  display:inline-block;
  line-height:44px;
  width:44px;
  height:44px;
  text-align:center;
  margin-right:8px;
  border-radius:100%;
  -webkit-transition:all .2s linear;
  -o-transition:all .2s linear;
  transition:all .2s linear
}
.social-icons a:active,.social-icons a:focus,.social-icons a:hover
{
  color:#fff;
  background-color:#29aafe
}
.social-icons.size-sm a
{
  line-height:34px;
  height:34px;
  width:34px;
  font-size:14px
}
.social-icons a.facebook:hover
{
  background-color:#3b5998
}
.social-icons a.twitter:hover
{
  background-color:#00aced
}
.social-icons a.linkedin:hover
{
  background-color:#007bb6
}
.social-icons a.dribbble:hover
{
  background-color:#ea4c89
}
@media (max-width:767px)
{
  .social-icons li.title
  {
    display:block;
    margin-right:0;
    font-weight:600
  }
}
</style>
<script>
$('body,html').click(function(e){
    $('.sidebar').toggleClass('sidebar');
});
</script>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="./dashboard.php"><img src="../assets/images/biller_logo.png" style="width:200px; height:140px; padding-bottom: 60px;" alt="logo"/></a>
      <!--   <a class="navbar-brand brand-logo-mini" href="../../index.html"><img src="../../images/logo-mini.svg" alt="logo"/></a> -->
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
<!--   <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button> -->
        <ul class="navbar-nav">
          <li class="nav-item dropdown d-none d-lg-flex">
            <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="" data-toggle="dropdown">
              <span class="btn">+ Add New</span>
            </a>
            <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">
              <a class="dropdown-item" href="category.php">
                <i class="icon-user text-primary"></i>
                Add Category
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="customer.php">
                <i class="icon-user-following text-warning"></i>
                Add Customer
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="addsupplier.php">
                <i class="icon-docs text-success"></i>
                Add Supplier
              </a>

            <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="cushistory.php">
                <i class="icon-user-following text-warning"></i>
                Customer List
              </a>
            <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="supplierlist.php">
                <i class="icon-user-following text-warning"></i>
                Supplier List
              </a>
            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">

          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="icon-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left"> <?=$shopname?>
                </p>
                <span class="badge badge-pill badge-warning float-right">Shop</span>
              </a>
              <a href="profile.php" class="dropdown-item">
              <p class="mb-0 font-weight-normal float-left"><i class="fa fa-user"></i>Profile </p> </a>
 
              <a href="password.php" class="dropdown-item">
              <p class="mb-0 font-weight-normal float-left"><i class="fa fa-undo"></i>Change Password </p> </a>

                <a href="logout.php" class="dropdown-item">
              <p class="mb-0 font-weight-normal float-left"><i class="fa fa-sign-out"></i>Log out </p> </a>   

            </div>
          </li>

          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <!-- partial:../../partials/_settings-panel.html -->
<!--         <div class="theme-setting-wrapper">
          <div id="settings-trigger"><i class="mdi mdi-settings"></i></div>
          <div id="theme-settings" class="settings-panel">
            <i class="settings-close mdi mdi-close"></i>
            <p class="settings-heading">SIDEBAR SKINS</p>
            <div class="sidebar-bg-options selected" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
            <div class="sidebar-bg-options" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
            <p class="settings-heading mt-2">HEADER SKINS</p>
            <div class="color-tiles mx-0 px-4">
              <div class="tiles primary"></div>
              <div class="tiles success"></div>
              <div class="tiles warning"></div>
              <div class="tiles danger"></div>
              <div class="tiles pink"></div>
              <div class="tiles info"></div>
              <div class="tiles dark"></div>
              <div class="tiles default"></div>
            </div>
          </div>
        </div> -->
        <div id="right-sidebar" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <ul class="nav nav-tabs" id="setting-panel" role="tablist">
<!--             <li class="nav-item">
              <a class="nav-link active" id="todo-tab" data-toggle="tab" href="home.php" role="tab" aria-controls="todo-section" aria-expanded="true">Dashboard</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">Sidebar</a>
            </li>
          </ul>

        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                <div class="profile-image">
                  <img src="../assets/images/users/avatar-1.jpg" alt="image"/>
                  <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
                </div>
                <div class="profile-name">
                  <p class="designation">
                    Super Admin
                  </p>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="home.php">
                <i class="icon-rocket menu-icon"></i>
                <span class="menu-title">Dashboard</span>
                <span class="badge badge-success">New</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#page-layouts" aria-expanded="false" aria-controls="page-layouts">
                <i class="icon-check menu-icon"></i>
                <span class="menu-title"> Billing </span>
              </a>
              <div class="collapse" id="page-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="dashboard.php">Sales billing</a></li>
                  <li class="nav-item"> <a class="nav-link" href="purchase.php">Purchase Billing</a></li>
                  <li class="nav-item"> <a class="nav-link" href="salesreturn.php">Sales Return</a></li>
                  <li class="nav-item"> <a class="nav-link" href="purchasereturn.php">Purchase Return</a></li>
                </ul>
              </div>
            </li>
              
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false" aria-controls="sidebar-layouts">
                <i class="icon-layers menu-icon"></i>
                <span class="menu-title"> Reports</span>
              </a>
              <div class="collapse" id="sidebar-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="billinghistory.php">Sales Reports</a></li>
                  <li class="nav-item"> <a class="nav-link" href="purchasehistory.php"> Purchase Report </a></li>
                  <li class="nav-item"> <a class="nav-link" href="salesreturnhistory.php">Sales Return</a></li>
                  <li class="nav-item"> <a class="nav-link" href="purchasereturnhistory.php">Purchase Return</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="icon-target menu-icon"></i>
                <span class="menu-title"> Stocks </span>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="stocks.php">Stock Report</a></li>
                  <li class="nav-item"> <a class="nav-link" href="outofstocks.php">Out of Stocks </a></li>
                </ul>
                </div>
            </li>



            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title"> Tax Report </span>
              </a>
              <div class="collapse" id="tables">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="puchasevat.php"> Purchase Tax </a></li>
                  <li class="nav-item"> <a class="nav-link" href="salesvat.php"> Sales Tax </a></li>
                </ul>
              </div>
            </li>
  
              
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#icons" aria-expanded="false" aria-controls="icons">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title"> Accounts </span>
              </a>
              <div class="collapse" id="icons">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="groupheads.php">Group Heads </a></li>
                  <li class="nav-item"> <a class="nav-link" href="ledger.php"> Ledger </a></li>
                   <li class="nav-item"> <a class="nav-link" href="expense.php"> Reciepts and Vouchers </a></li>
                  <li class="nav-item"> <a class="nav-link" href="daybooks.php"> Financial statements </a></li>              
                   <li class="nav-item"> <a class="nav-link" href="profit.php"> Profit </a></li>    
                </ul>
              </div>
            </li>
<?php 

$today = date('Y-m-d');             
$todybil = $conn->query("SELECT be_billid FROM vm_billentry WHERE user_id='".$_SESSION["admin"]."' AND DATE(be_billdate)='$today' AND be_isactive='0'");

?>
            <li class="nav-item nav-doc">
              <a class="nav-link bg-primary" href="../documentation.html">
                <i class="icon-magnet menu-icon"></i>
                <span class="menu-title">Documentation</span>
              </a>
            </li>
            <li class="mt-4 nav-item nav-progress">
              <h6 class="nav-progress-heading mt-4 font-weight-normal">
                Today's Sales
                <span class="nav-progress-sub-heading">
                 <?= mysqli_num_rows($todybil) ?> sold
                </span>
              </h6>
              <div class="progress progress-sm">
                <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <h6 class="nav-progress-heading mt-4 font-weight-normal">
                Customer target
                <span class="nav-progress-sub-heading">
                  500
                </span>
              </h6>
              <div class="progress progress-sm">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </li>
          </ul>
        </nav>

        </div>
        </div>
