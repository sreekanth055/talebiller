<?php
$page = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);

?>
<style>
.menu.accordion-menu>li>a, body:not(.page-horizontal-bar):not(.small-sidebar) .menu.accordion-menu a {
    text-align: left;
}
@media (min-width:768px){
	#activebar{display:none;}
}
</style>
<div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    
                    <ul class="menu accordion-menu">
                    	<?php 
						   if($active==0)
							{?><li id="activebar" style="background: red;"><a href="#" data-toggle="modal" data-target="#myModal_msg" class="waves-effect waves-button"><p style="color: white;">Activate Your Account.<br />trial ends in <?=$endtrail?> day</p></a></li><?php }?>
                    	<li <?php if($page == 'home.php'){ ?> class="active" <?php } ?>><a href="home.php" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-dashboard"></span> Dashboard</p></a>   
                        </li>
                    	<li class="droplink <?php if($page == 'dashboard.php' || $page == 'purchase.php' || $page == 'salesreturn.php' || $page == 'purchasereturn.php' || $page == 'estimation.php'){ ?>active open<?php } ?>"><a href="#" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-print"></span> Billing</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li<?php if($page == 'dashboard.php'){ ?> class="active" <?php } ?>><a href="dashboard.php">Sales Billing</a></li>
                                
                                <li<?php if($page == 'purchase.php' ){ ?> class="active" <?php } ?>><a href="purchase.php">Purchase Billing</a></li>
                                
                                <li<?php if($page == 'salesreturn.php' ){ ?> class="active" <?php } ?>><a href="salesreturn.php">Sales Return</a></li>
                                
                                <li<?php if($page == 'purchasereturn.php' ){ ?> class="active" <?php } ?>><a href="purchasereturn.php">Purchase Return</a></li>
                                
                              <!--  <li<?php if($page == 'estimation.php' ){ ?> class="active" <?php } ?>><a href="estimation.php">Estimation</a></li>-->
                                
                            </ul>
                        </li>
                        <li class="droplink <?php if($page == 'billinghistory.php' || $page == 'salesreturnhistory.php' || $page == 'purchasereturnhistory.php' || $page == 'purchasehistory.php' || $page == 'estimationhistory.php'){ ?>active open<?php } ?>"><a href="#" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-book"></span> Reports</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li<?php if($page == 'billinghistory.php'){ ?> class="active" <?php } ?>><a href="billinghistory.php">Sales Report</a></li>
                                
                                <li<?php if($page == 'purchasehistory.php'){ ?> class="active" <?php } ?>><a href="purchasehistory.php">Purchase Report</a></li>
                                
                                <li<?php if($page == 'salesreturnhistory.php'){ ?> class="active" <?php } ?>><a href="salesreturnhistory.php">Sales Return</a></li>
                                
                                <li<?php if($page == 'purchasereturnhistory.php'){ ?> class="active" <?php } ?>><a href="purchasereturnhistory.php">Purchase Return</a></li>
                                
                               <!-- <li<?php if($page == 'estimationhistory.php'){ ?> class="active" <?php } ?>><a href="estimationhistory.php">Estimations</a></li> -->
                            </ul>
                        </li>
                        <li class="droplink <?php if($page == 'stocks.php' || $page == 'addstocks.php' || $page == 'outofstocks.php'){ ?>active open<?php } ?>"><a href="#" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-folder-open"></span> &nbsp;Stocks</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li<?php if($page == 'stocks.php'){ ?> class="active" <?php } ?>><a href="stocks.php">Stock Report</a></li>
                                
                                <li<?php if($page == 'outofstocks.php'){ ?> class="active" <?php } ?>><a href="outofstocks.php">Out of Stocks</a></li>
                                
                            </ul>
                        </li>
                        <li class="droplink <?php if($page == 'puchasevat.php' || $page == 'salesvat.php'){ ?>active open<?php } ?>"><a href="#" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-briefcase"></span> Tax Report</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li<?php if($page == 'puchasevat.php'){ ?> class="active" <?php } ?>><a href="puchasevat.php">Purchase TAX</a></li>
                                
                                <li<?php if($page == 'salesvat.php'){ ?> class="active" <?php } ?>><a href="salesvat.php">Sales TAX</a></li>
                                
                            </ul>
                        </li>
                        
                   
                        
                        
                           
                        </li>
                             
                        </li>
						<li <?php if($page == 'expense.php'){ ?> class="active" <?php } ?>><a href="expense.php" class="waves-effect waves-button"><p><span class="menu-icon fa fa-money"></span> Expense</p></a>   
                        </li>
                        
                         <li <?php if($page == 'daybook.php'){ ?> class="active" <?php } ?>><a href="daybook.php" class="waves-effect waves-button"><p><span class="menu-icon 	glyphicon glyphicon-time"></span> Daybook</p></a>   
                        </li>
                        
                        <li <?php if($page == 'ledger.php'){ ?> class="active" <?php } ?>><a href="ledger.php" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-book"></span> Ledger</p></a>   
                        </li>
                        
                        <li class="droplink <?php if($page == 'category.php' || $page == 'customer.php' || $page == 'addsupplier.php' || $page == 'cushistory.php' || $page == 'supplierlist.php'){ ?>active open<?php } ?>"><a href="#" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-cog"></span> Settings</p><span class="arrow"></span></a>
                            <ul class="sub-menu">
                                <li<?php if($page == 'category.php'){ ?> class="active" <?php } ?>><a href="category.php">Add Category</a></li>
                                <li<?php if($page == 'customer.php'){ ?> class="active" <?php } ?>><a href="customer.php">Add Customer</a></li>
                                <li<?php if($page == 'addsupplier.php'){ ?> class="active" <?php } ?>><a href="addsupplier.php">Add Supplier</a></li>
                                <li<?php if($page == 'cushistory.php'){ ?> class="active" <?php } ?>><a href="cushistory.php">Customer List</a></li>
                                <li<?php if($page == 'supplierlist.php'){ ?> class="active" <?php } ?>><a href="supplierlist.php">Supplier List</a></li>
                              <!--  <li<?php if($page == 'barcodemenu.php'){ ?> class="active" <?php } ?>><a href="barcodemenu.php">Barcode</a></li>-->
                                <li> <a onClick="return confirm('Do you want to Backup?')" href="backup.php?back=<?=$page?>">Backup</a></li>
                                
                                
                            </ul>
                        </li>
                         
						
						
                          
						
                        
						
                        
                        <li <?php if($page == 'profile.php'){ ?> class="active" <?php } ?>><a href="profile.php" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-user"></span> Profile</p></a></li>
                        

						<li <?php if($page == 'logout.php'){ ?> class="active" <?php } ?>><a href="logout.php" class="waves-effect waves-button"><p><span class="menu-icon glyphicon glyphicon-off"></span> Logout</p></a></li>
                      
		
						
					  
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div>
            <script>
/*window.onload=function(){
	var shopid=<?=$_SESSION['admin']?>;
	
   
    $.post("deactivate.php", {shopid: shopid}, function(result){
        if(result=="success")
		{
			window.location.replace("../index.php");
			//alert("hai");
		}elseif(result=="failed")
		{
			alert("Please Contact Us : 7293404311");
		}
    });
	
	}*/
	function deactivate()
	{
		
		var active=<?=$active?>;
		var endtrail=<?=$endtrail?>;
		if(active==0){if(endtrail<=0){
		var http = new XMLHttpRequest();
var url = "deactivate.php";
var params = "shopid=<?=$_SESSION["admin"]?>";
http.open("POST", url, true);

//Send the proper header information along with the request
http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

http.onreadystatechange = function() {//Call a function when the state changes.
    if(http.readyState == 4 && http.status == 200) {
        if(http.responseText=='success')
		{
			window.location.replace("../index.php?endtrail=1");
		}else
		{
			alert("Please Contact Us : 7293404311");
		}
    }
}
http.send(params);
		}else{return;}}else{return;}}
</script>
